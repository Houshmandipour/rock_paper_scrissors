from datetime import datetime


def log_time(func):
    def wrapped_func(*args, **kwargs):
        start_time = datetime.now()
        result = func(*args, **kwargs)
        end_time = datetime.now()
        duration = end_time - start_time
        hours = duration.seconds//3600
        min = duration.seconds//60
        sec = duration.seconds % 60
        print(f"Total time : {hours} : {min} : {sec}")

        return result
    return wrapped_func
